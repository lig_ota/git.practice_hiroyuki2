#宿題内容です

第１回お疲れ様でした。

* [自分の名前]ブランチを作成して、`lesson1/home_work`フォルダ内に[自分の名前].htmlを作成して下さい。
* ブランチを作ったらpushまでして下さい。
* bitbucket上にブランチが表示されていればOKです ([ここ](https://bitbucket.org/n0bisuke/git.practice/branches)から確認できます。)
* ここで作ったブランチをもとに、第２回のブランチマージを進めて行きます。
(原則ですが、ローカルリポジトリは最新の状態(pull)にしてから作業しましょう!)

例 n0bisuke.html

    <!DOCTYPE html>
    <html lang="ja">
    <head>
	    <meta charset="UTF-8">
	    <title>Document</title>
    </head>
    <body>
     <h1>第１回について一言!</h1>
      暑かった。
     <h1>聞きたいこと</h1>
      前のバージョンに戻すにはどうしたら良いですか?
    </body>
    </html>
