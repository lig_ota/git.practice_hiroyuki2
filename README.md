#デザイナー向けgit講座
 
##第１回 2014/6/20(金) AM11:00〜12:00
[lesson1](https://bitbucket.org/n0bisuke/git.practice/src/0077151930fb42f541a294fbab7e5fb13e859e10/lesson1/README.md?at=master)

##第２回 2014/6/27(金) AM11:00〜12:00
[lesson2](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson2/README.md?at=master)

##第3回 2014/7/18(金)  AM11:00〜12:00
[lesson3](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson3/README.md?at=master)

##第4回  2014/7/25(金)  AM11:00〜12:00
[lesson4](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson4/README.md?at=master)

##第5回 2014/8/1(金) AM11:00〜12:00
[lesson5](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson5/README.md?at=master)

##第6回 2014/8/8(金) AM11:00〜12:00
[lesson6](https://bitbucket.org/n0bisuke/git.practice/src/HEAD/lesson6/README.md?at=master)

#役立ちリンク
* [本当は怖くない！デザイナーがGitを大好きになった♡５つの理由](http://nanapi.co.jp/blog/2014/04/23/git-love/)
